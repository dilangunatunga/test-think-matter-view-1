<?php


namespace App\Services\Auth;
use Illuminate\Support\Facades\DB;
use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\UserToken;

class CognitoAuthenticate
{
    private $cognitoClient;
    public $tokenData;

    public function __construct()
    {
        $cognitoClient = new CognitoIdentityProviderClient([
            'version'     => 'latest',
            'region'      => 'ap-southeast-2',
            'credentials' => [
                'key'    => env('AWS_KEY'),
                'secret' => env('AWS_SECRET'),
            ],
        ]);
        $this->cognitoClient = $cognitoClient;


    }

    public function refreshCognitoTokens(String $refreshToken)
    {

        try {

            $response = $this->cognitoClient->adminInitiateAuth([
                'AuthFlow' => 'REFRESH_TOKEN_AUTH',
                'AuthParameters' => [
                    'REFRESH_TOKEN' => $refreshToken,
                ],
                'ClientId' => config('cognito.client-id'),
                'UserPoolId' => config('cognito.pool-id'),
            ]);

            return $response['AuthenticationResult'];

        } catch (CognitoIdentityProviderException $e) {

        }




    }



    public function _cognitoLoginRequest(array $credentials){


        if(empty($credentials['username']) || empty($credentials['password']))
            return false;

        $username = $credentials['username'];
        $password = $credentials['password'];

        try {

            $response = $this->cognitoClient->adminInitiateAuth([
                'AuthFlow' => 'ADMIN_NO_SRP_AUTH',
                'AuthParameters' => [
                    'USERNAME' => $username,
                    'PASSWORD' => $password,
                ],
                'ClientId' => config('cognito.client-id'),
                'UserPoolId' => config('cognito.pool-id'),
            ]);


            $responseData = $response->toArray();

            return $responseData;
        }catch (CognitoIdentityProviderException $e) {
            //activity log goes here

            return false;
        }
        return false;
    }


    public function updatePassword($username,$password){

        try {
            $this->cognitoClient->adminSetUserPassword([
                'Password' => $password, // REQUIRED
                'Permanent' => true,
                'UserPoolId' => config('cognito.pool-id'),
                'Username' => $username, // REQUIRED
            ]);
        }
        catch(CognitoIdentityProviderException $e){

            return false;
        }

    }

    public function cognitoLogin(array $credentials){


            $responseData = $this->_cognitoLoginRequest($credentials);

            if(!empty($responseData) && !empty($responseData['AuthenticationResult'])){
                $this->tokenData = $responseData['AuthenticationResult'];

                if($this->decodeIdToken() !== false && hash_equals(env('AWS_COGNITO_USER_POOL_PUBLIC_KEY'),$this->tokenData['jwt_private_key']->kid) )
                    return $responseData;
                //error
                //activity log goes here
                unset($this->tokenData);
                return false;

            }else
                return false;


    }

    public function decodeIdToken(){

        if(empty($this->tokenData['IdToken'])) {
            return false;
        }

        $this->tokenData['jwt_private_key'] = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $this->tokenData['IdToken'])[0]))));
        $this->tokenData['jwt_payload'] = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $this->tokenData['IdToken'])[1]))));

        return true;

    }

    public function registerUser(){

        $cognito_user_id = $this->tokenData['jwt_payload']->sub;

        $user = User::where('cognito_id', $cognito_user_id)->first();

        if(!empty($user->id)){
            $tokenData = $this->tokenData;
            $tokenData['user_id'] = $user->id;
            return $this->createTokenEntry($tokenData);

        }

        return false;
    }

    public function createTokenEntry($tokenData){

        $date = new \DateTime('+1 hours');

        $userToken = new UserToken();
        $userToken->user_id = $tokenData['user_id'];
        $userToken->access_token = $tokenData['AccessToken'];
        $userToken->id_token = $tokenData['IdToken'];
        $userToken->refresh_token = $tokenData['RefreshToken'];
        $userToken->expire_at = $date->format('Y-m-d H:i:s');
        $userToken->status = 1;
        $userToken->save();

        return $userToken->id;
    }

    public function processLogin(array $credentials){
        $user_token_id = false;

        $res = $this->cognitoLogin($credentials);

        if($res)
            $user_token_id = $this->registerUser();

        if($res && $user_token_id)
            return $this->tokenData;


    }
    public function getUserByToken(){


    }



}
