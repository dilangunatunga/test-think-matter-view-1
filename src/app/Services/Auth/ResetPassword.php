<?php

namespace App\Services\Auth;
use App\Models\User;
use App\Models\UserPasswordReset;
use App\Models\UserPasswordResetAttempts;
use App\Models\UserSecurityQuestionAnswers;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;


class ResetPassword
{

    function _getUser($email){
        //DB::enableQueryLog();
        $users = User::where('email',$email)
            ->join('user_type_links','users.id','=','user_type_links.user_id')
            ->where('user_type_id','1 ')  //staff
            ->whereNull('user_type_links.deleted_at')
            ->get();
        //dd(DB::getQueryLog());
        if(count($users) != 1)
            return false;

        return Arr::first($users);

    }

    function _createResetPasswordEntry($user){

        $userPasswordReset = new UserPasswordReset();
        $userPasswordReset->user_id = $user->user_id;
        $userPasswordReset->code = Str::random(16);
        $userPasswordReset->save();

        return $userPasswordReset;

    }

    function processPasswordResetRequest($email){

        if($user = $this->_getUser($email)){
            $userPasswordReset = $this->_createResetPasswordEntry(($user));
            if(!empty($userPasswordReset->id)){
                $this->emailResetPassword($user,$userPasswordReset);
                return true;

            }

            return false;

        }

        return false;
    }

    function emailResetPassword(User $user,UserPasswordReset $userPasswordReset){

        // temporary email solution. Should use email micro service container to send out emails

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "cs@conveyancing.com.au";
        $mail->Password = 'vc~Xc{K5?w#dK"Ld';
        $mail->setFrom('cs@conveyancing.com.au', 'Conveyancing.com.au Customer Support');
        $mail->Subject = "Password Reset Request";

        $content = 'Hi '.$user->firstname.',<br><br>
                    We have been notified that you have forgotten the password for your account.<br>Please click on the following link and you will be prompted to enter new password details.
                    ';
        $content .= '<br><br><a href="'.config('app.front_end_lik').'resetpasswordsecurity?ry='.$userPasswordReset->code.'">Reset Password</a>';
        $mail->msgHTML($content);
        // add client2's email

        // add client's email
        $mail->addAddress($user->email);

        if (!$mail->send()) {
            return false;
        }
        return true;



    }

        function _checkResetPassword($code){
//DB::enableQueryLog();
        $date = new \DateTime();
        $date->modify('-24 hours');
        $userPasswordReset = UserPasswordReset::where([
                                ['code','=',$code],
                                ['status','=',0]
                            ])
                            ->whereNull('reset_at')
                            ->where('created_at', '>=', $date->format('Y-m-d H:i:s'))
                            ->first();
//dd(DB::getQueryLog());
        if(!empty($userPasswordReset->user_id))
            return $userPasswordReset;
        else
            return false;


    }

    function _checkSecurityQuestion($userId,$questionId,$answer){

        return UserSecurityQuestionAnswers::where([
                                            ['user_id','=',$userId],
                                            ['question_id','=',$questionId],
                                            ['answer','=',$answer]
                                            ])->whereNull('deleted_at')->exists();

    }

    function updateVerificationCode($userPasswordReset){

        $userPasswordReset->validation_code = Str::random(16);
        $userPasswordReset->save();

        return $userPasswordReset;
    }

    function validateSecurityQuestion($request){

        if($userPasswordReset = $this->_checkResetPassword($request->input('code'))){

            if($this->checkFailedAttempts($userPasswordReset->user_id) >=3)
                return false;

            if($this->_checkSecurityQuestion($userPasswordReset->user_id,$request->input('id'),$request->input('answer'))) {
                $userPasswordReset = $this->updateVerificationCode($userPasswordReset);
                $this->deleteFailedAttempts($userPasswordReset);
                return $userPasswordReset ?? false;
            }
            else{

                $this->recordFailedAttempt($userPasswordReset);

            }
        }else
            return false;

    }
    function checkFailedAttempts($user_id){
        return UserPasswordResetAttempts::where("user_id",$user_id)->whereNull('deleted_at')->count();
    }


    function deleteFailedAttempts($userPasswordReset){
        UserPasswordResetAttempts::where("user_id",$userPasswordReset->user_id)->whereNull('deleted_at')->update(['deleted_at'=>date('Y-m-d H:i:s')]);

    }

    function recordFailedAttempt($userPasswordReset){

        $userPasswordResetAttempts = new UserPasswordResetAttempts();
        $userPasswordResetAttempts->user_id = $userPasswordReset->user_id;
        $userPasswordResetAttempts->save();
    }

    function validateVerificationCode($code,$validationCode){

        $userPasswordReset = userPasswordReset::where([
                                    ['code','=',$code],
                                    ['validation_code','=',$validationCode],
                                    ['status','=',0]
                                ])->whereNull('reset_at')->first();



        if(!empty($userPasswordReset) && !empty($userPasswordReset->user_id))
            return $userPasswordReset;
        else
            return false;

    }


}
