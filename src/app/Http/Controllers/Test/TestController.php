<?php

    namespace App\Http\Controllers\Test;

    use App\Http\Controllers\Controller;
    use App\Services\Auth\CognitoAuthenticate;
    use Illuminate\Foundation\Auth\AuthenticatesUsers;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
    use Illuminate\Http\Request;
    use Carbon\Carbon;

    class TestController extends Controller{


        public function pets(){
            printf("Now: %s", Carbon::now());

            $pets = DB::table('pet')->get();
            print_r($pets);

        }

        public function demo(){

            $date = new \DateTime();
            print $date->format('Y-m-d H:i:s');
            $date->modify('-24 hours');
            print $date->format('Y-m-d H:i:s');
            return response()->json([
                'responseText' => 'Perfect',
                'messages' => ['global_errors'=>'Invalid Password'],
                'status' => 'Fail'
            ],200);
        }

        public function token(){


            $client = new CognitoIdentityProviderClient([
                'version'     => 'latest',
                'region'      => 'ap-southeast-2',
                'credentials' => [
                    'key'    => 'AKIAV553VUBFOBUKWAVE',
                    'secret' => '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',
                ],
            ]);
            $result = array();

          //  $id_token = 'eyJraWQiOiJ6RGNKQzEwOExcL0UyQm1Gb010T3BwZnB6ckFGczZvTzV4WG1CR3lQZ1IxYz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJkM2Q3ZGY4ZS0wYmU5LTQyYTMtOTcwZi1iMDdlOTIxMjIxNTQiLCJhdWQiOiI1bTE4MHZxcGV0aHZwMmVnanQyaW4yczZ1aiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjFmZjBmMzgwLTRhNmYtNDg4YS04YTA5LTJhOTEyYzI3ZGIxMiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTc4NTQ2MDI0LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuYXAtc291dGhlYXN0LTIuYW1hem9uYXdzLmNvbVwvYXAtc291dGhlYXN0LTJfUGtYOHY4MEpyIiwibmFtZSI6ImRpbGFuIiwiY29nbml0bzp1c2VybmFtZSI6ImRrZyIsImV4cCI6MTU3ODU0OTYyNCwiaWF0IjoxNTc4NTQ2MDI0LCJlbWFpbCI6ImRpbGtnMTAwQGdtYWlsLmNvbSJ9.Z9xn-QH0jBipyImJbcgC4JbUQOd__WnyzAOaGRcOZfploQgTPYMGgAM06izosZdALubKXSssIvqCNVPJu0oxSIActKWa4CsG7QsTj0Mso6i1y-ZWskVFwNDdLNNPnJK1orPGWt5U4TPWDgofx2SZ0MhK_JWKgSWTB11eVwvuGZ5NgDj3yjuhDO-NlDVLZmXvMFRg0MJJVEfsGxiOZyQvCEXokBap7mp9Cm_kIX2bpvapU5LC9Kv1ipb17UzO31HS9F4ANX9rkz4t1oD5SASLqueDP65tBV34C_a2DHjRp1ZqDpdAHLgYL8_qv5VPauzYvXOTxYOKaJHnIgijiRB7Gw';
           // $access_token = 'eyJraWQiOiJvczFLd3RvNDZ4a0tvQXZqZ3VUMU5WODJXRThPT1M0c2Njd3h6ckhxWUlzPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJkM2Q3ZGY4ZS0wYmU5LTQyYTMtOTcwZi1iMDdlOTIxMjIxNTQiLCJldmVudF9pZCI6IjFmZjBmMzgwLTRhNmYtNDg4YS04YTA5LTJhOTEyYzI3ZGIxMiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1Nzg1NDYwMjQsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3QtMi5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMl9Qa1g4djgwSnIiLCJleHAiOjE1Nzg1NDk2MjQsImlhdCI6MTU3ODU0NjAyNCwianRpIjoiOGQ3Y2ZmNGUtZDU2My00N2ZiLWFiYWEtMWNkZTllNWFhODY4IiwiY2xpZW50X2lkIjoiNW0xODB2cXBldGh2cDJlZ2p0MmluMnM2dWoiLCJ1c2VybmFtZSI6ImRrZyJ9.n7k_RseVNYEZPVLrY8J0eF_WcC-_zcFecvWMU6Kbw0mQU5SKOlkdshV0kx1AKhIWQAO-ymIcBbHN8SntweJmrxhjGLI8epQbs-m28vknJTjE2ipqeRbbfftbsc7jpgD4GsN4v43HFdxMuy2BK7hJwvh865prAT7l3tUno2FxrHMqcRcCJFJyTV4cElQCaPelxPQRLiXcsh4o-dr8Qg9HHbxRkkpZPoRk0y9sSK4Q9P5PNanKpLp_7fqz5jJvinbJ0Br_JHLw8aLNRU6CsR6GjYX_G8HoJ48R7dNYQgkjAfCX5aF-IWQ7lhoKbp8urcYSSXXobKSJ8Ay8vJ-dwaWA6Q';
            $access_token   = $_COOKIE['accessToken2'];
            $id_token       = $_COOKIE['idToken2'];
            if(!empty($access_token)) {
                $result = $client->getUser([
                    'AccessToken' => $access_token, // REQUIRED
                ]);
            }


            $key = file_get_contents('https://cognito-idp.ap-southeast-2.amazonaws.com/ap-southeast-2_PkX8v80Jr/.well-known/jwks.json');

            $data_array = array();
           // print_r(json_decode($key));
            $data_array['cognito_public_key'] = json_decode($key);

            //print_r(json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $id_token)[0])))));
            $data_array['jwt_private_key'] = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $id_token)[0]))));

            //print_r(json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $id_token)[1])))));
            $data_array['jwt_payload'] = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $id_token)[1]))));


            $data_array['php_verified_data'] = $result;
print '<pre>';
            print_r($data_array);
 print '</pre>';

        }

        function login(){

            return view('login');

        }

        function req(){

           // print_r($_COOKIE);

/*
            $opts = array(
                'http'=>array(
                    'method'=>"GET",
                    'header'=>"Accept-language: en\r\n" .
                        "Authorization: ".$_COOKIE['idToken2']."\r\n" .
                        "Token: ".$_COOKIE['accessToken2']."\r\n" .
                        "User-agent: BROWSER-DESCRIPTION-HERE\r\n"
                )
            );

            $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
            $response = file_get_contents('https://naynhjgpo6.execute-api.ap-southeast-2.amazonaws.com/test2/access/access2', false, $context);
*/
            $curl_h = curl_init('https://naynhjgpo6.execute-api.ap-southeast-2.amazonaws.com/test2/access/access2');

            curl_setopt($curl_h, CURLOPT_HTTPHEADER,
                array(
                    'User-Agent: NoBrowser v0.1 beta',
                    "Authorization: ".$_COOKIE['idToken2'],
                    "Token: ".$_COOKIE['accessToken2']

                )
            );

# do not output, but store to variable
            curl_setopt($curl_h, CURLOPT_RETURNTRANSFER, false);

            $response = curl_exec($curl_h);

            print_r($response);
        }

        function loginCognito(Request $request){

            $credentials['username'] = $request->input('username');
            $credentials['password'] = $request->input('password');

            $cognitoAuth = new CognitoAuthenticate();


            $res = $cognitoAuth->processLogin($credentials);

            if($res){

                print_r($res);
                print 'login worked';
            }
            else
                print 'login error';
        }


        function resetPassword(Request $request){

            $user = Auth::user();
            print_r($user);
            $password = $request->input('password');
            $cognitoAuth = new CognitoAuthenticate();
            $res = $cognitoAuth->updatePassword($username,$password);


        }



    }

