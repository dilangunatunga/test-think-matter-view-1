<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Services\Auth\CognitoAuthenticate;
use Illuminate\Http\Request;
use App\Models\UserToken;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('guest')->except('logout');
    }

    function loginCognito(Request $request){


        $credentials['username'] = strtolower($request->input('username'));
        $credentials['password'] = $request->input('password');
        $credentials['remember'] = $request->input('remember');

        $cognitoAuth = new CognitoAuthenticate();
        $res = $cognitoAuth->processLogin($credentials);

        if($res){

            return response()->json([
                'data' => '',
                'AccessToken'=>$res['AccessToken'],
                'status' => 'Success'
            ],200);
        }
        else{

            return response()->json([
                'data' => '',
                'messages' => ['global_errors'=>'The Username or Password was incorrect'],
                'status' => 'Fail'
            ],401);

        }

    }

    function logout(Request $request){

        $token = UserToken::where("access_token",$request->input('accessToken'))->first();
        $token->delete();

        return response()->json([
            'data' => '',
            'status' => 'Success'
        ],200);
    }

}
