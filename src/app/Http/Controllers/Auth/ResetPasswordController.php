<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Services\Auth\CognitoAuthenticate;
use App\Services\Auth\ResetPassword;
use Illuminate\Http\Request;


class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    function resetPasswordRequest(Request $request){

        $email = strtolower($request->input('email'));
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){

            return response()->json([
                'data' => '',
                'messages' => ['global_errors'=>'Invalid Email'],
                'status' => 'Fail'
            ],401);
        }


        $resetPassword = new ResetPassword();

        $resetPassword->processPasswordResetRequest($email);

        return response()->json([
            'data' => '',
            'status' => 'Success'
        ],200);
    }

    function validateResetPassword(Request $request){

        $resetPassword = new ResetPassword();
        if($userPasswordReset = $resetPassword->_checkResetPassword($request->input('code'))){
            return response()->json([
                 'messages' => '',
                'status' => 'Success'
            ],200);

        }else{
            return response()->json([
                'data' => '',
                'messages' => ['global_errors'=>'Invalid reset password link'],
                'status' => 'Fail'
            ],401);

        }

    }

    function validateSecurityQuestion(Request $request){

        $resetPassword = new ResetPassword();
        if($userPasswordReset = $resetPassword->validateSecurityQuestion($request)){
            return response()->json([
                'data' => ['verificationCode'=>$userPasswordReset->validation_code],
                'messages' => '',
                'status' => 'Success'
            ],200);

        }else{
            return response()->json([
                'data' => '',
                'messages' => ['global_errors'=>'Reset Password Fail'],
                'status' => 'Fail'
            ],401);

        }

    }


    function resetPassword(Request $request){

        if(preg_match("/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[^\w]).*$/", $request->input('password')) === 0){

            return response()->json([
                'data' => '',
                'messages' => ['global_errors'=>'Invalid Password'],
                'status' => 'Fail'
            ],401);
        }



        $resetPassword = new ResetPassword();

        if($userPasswordReset = $resetPassword->validateVerificationCode($request->input('code'),$request->input('validationCode'))) {

            $user = User::find($userPasswordReset->user_id);

            //Check if the new password is equal to new password
            $credentials = array('username'=>$user->email,'password'=>$request->input('password'));
            $cognitoAuth = new CognitoAuthenticate();
            $loginRes = $cognitoAuth->_cognitoLoginRequest($credentials);

            if( $loginRes !== false){ //if the new password is same as old password, return an error

                return response()->json([
                    'data' => '',
                    'messages' => ['global_errors'=>'Invalid password. New password should be different from your last password'],
                    'status' => 'Fail'
                ],401);
            }
            $res = $cognitoAuth->updatePassword($user->email, $request->input('password'));

            if($res !== false){

                $userPasswordReset->reset_at = date('Y-m-d H:i:s');
                $userPasswordReset->updated_at = date('Y-m-d H:i:s');
                $userPasswordReset->status = 1;
                $userPasswordReset->save();

                return response()->json([
                    'data' => '',
                    'status' => 'Success'
                ],200);

            }else{

                return response()->json([
                    'data' => '',
                    'messages' => ['global_errors'=>'Reset Password Fail.'],
                    'status' => 'Fail'
                ],401);
            }

        }else{

            return response()->json([
                'data' => '',
                'messages' => ['global_errors'=>'Reset Password Fail. Invalid User'],
                'status' => 'Fail'
            ],401);
        }

    }
}
