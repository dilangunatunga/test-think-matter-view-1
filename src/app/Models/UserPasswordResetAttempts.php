<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPasswordResetAttempts extends Model
{
    //
    use SoftDeletes;
    protected $table = 'user_password_reset_attempts';


}
