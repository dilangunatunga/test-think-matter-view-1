<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\UserToken;
use Carbon\Carbon;
use App\Services\Auth\CognitoAuthenticate;


class AuthServiceProvider extends ServiceProvider

{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::viaRequest('cognito-token', function ($request) {

            $userToken = UserToken::where('access_token', $request->token)->where('status', '1')->whereNull('deleted_at')->first();
            //print_r($user_token);
            if(!empty($userToken->user_id)){

                $expireDate = Carbon::parse($userToken->expire_at);
                var_dump($expireDate);
                $now = Carbon::now();
                var_dump($now);
                if($expireDate->lessThan($now)){

                    $cognitoAuth = new CognitoAuthenticate();
                    $tokenData = $cognitoAuth->refreshCognitoTokens($userToken->refresh_token);
                    $tokenData['user_id'] = $userToken->user_id;
                    $tokenData['RefreshToken'] = $userToken->refresh_token;

                    if(!empty($tokenData['AccessToken']))
                        $cognitoAuth->createTokenEntry($tokenData);
                    else
                        return false;


                }

                return User::find($userToken->user_id);
            }

            return User::find(0);

        });
        //
    }
}
