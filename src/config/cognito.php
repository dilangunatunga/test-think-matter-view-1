<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Pool ID
    |--------------------------------------------------------------------------
    |
    | This is the ID of your AWS Cognito User Pool.
    |
    */

    'pool-id' => env('AWS_COGNITO_USER_POOL_ID', ''),
    'client-id' => env('AWS_COGNITO_IDENTITY_APP_CLIENT_ID', ''),
    'refresh-token-expiration' => 30,
    'app' => 'default',

]


?>
