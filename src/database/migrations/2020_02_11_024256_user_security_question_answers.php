<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserSecurityQuestionAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_security_question_answers')) {
            Schema::create('user_security_question_answers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->unsignedBigInteger('question_id');
                $table->char('answer', '255');
                $table->softDeletes();
                $table->timestamps();
            });
        }

        Schema::table('user_security_question_answers', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('question_id')->references('id')->on('user_security_questions');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_security_question_answers');
    }
}
